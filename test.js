<script type="text/javascript">
    jQuery.get("https://freegeoip.app/json/", function (response) {
        jQuery("#ip").html("IP: " + response.ip);
        jQuery("#country_code").html(response.country_code);
        if(response.country_code=='CA') {
            document.getElementById('pricing-us-view').remove()
        }
        if (response.country_code=='US'){
            document.getElementById('pricing-ca-view').remove()
        }
    }, "jsonp");
    </script>